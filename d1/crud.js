// importing the "http" module
const http = require("http");

// storing the 400 in a variable called port
const port = 4000;

// Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
];



// storing the createServer method inside the server variable
const server = http.createServer((request, response) => {


	// route for returning all items upon receiving a GET method request
    // requests the "/users" path and "GET"s information
	if (request.url === "/users" && request.method === "GET" ) {
        // sets the status code to 200, meaning OK
        // sets the response output to JSON data type
        response.writeHead(200, { "Content-Type": "application/json" });
		// response.write()-write any specified string
        // since the needed data type is string, we use JSON.stringify to "write" it as our response
        // this is done because requests and responses sent between client and server requires the information to be sent and receive as a stringifed JSON
        response.write(JSON.stringify(directory));
        // ends the response process
		response.end();
	};
    if (request.url === "/users" && request.method === "POST"){
        let requestBody = "";
        /* 
        A stream is a sequence of data 
        data is recieved from the client and is processed in the "data" stream
        the information provided from the request object enters a sequence called "data" the code below will be triggered
            -"data" step - reads the "data" stream and processed it as the request body
        */
        request.on("data", function(data){
            // assigns the data retrieved from the data stream to requestBody
            requestBody += data;
        });
        // request end step - only runs after the request has completely been sent
        request.on("end",function(){
            // checks if at this point, the requestBody if off data type STRING 
            // we need this to be sure of data type JSON to access its properties
            console.log(typeof requestBody);

            // converts the string requestBody to JSON
            requestBody = JSON.parse(requestBody);

            // creating new object representing the new mock database record 
            let newUser = {
                "name":requestBody.name,
                "email": requestBody.email
            };

            // Adds the new user into the mock database
            directory.push(newUser);
            console.log(directory);


            response.writeHead(200, {"Content-Type":"application/json"});
            response.write(JSON.stringify(newUser));
            response.end();
        });
    };

});

// using server and port variables
server.listen(port);

// confirmation that the server is running
console.log(`Server now running at port: ${port}`);