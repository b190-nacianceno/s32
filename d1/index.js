
// importing the http module
const http = require("http");
// storing the 4000 in a variable called port
const port = 4000;

// storing the createServer method in a variable
const server = http.createServer((request,response) => {
    // HTTP Method can be accessed via "method" property inside the request object; they are to be written in ALL CAPS

    // GET REQUEST
    if ( request.url === "/items" && request.method === "GET" ) {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from the database");
	};
    // POST request - means that we will be adding/creating information
    if (request.url === "/items" && request.method === "POST"){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Data retrieved from the database");
    } 
    
}); 



/* 
GET method is one of the HTTPS methos that we will be using from this point.
    GET method means that we will be retrieving or reading information
*/

// using server and port variables
server.listen(port);

console.log("Server running at port: 4000");


